import { Component, OnInit, Input,OnChanges,ViewChild } from '@angular/core';
import { ChartType } from 'angular-google-charts';
import { GoogleChartInterface, GoogleChartType } from 'ng2-google-charts';





@Component({
  selector: 'app-piechart',
  templateUrl: './piechart.component.html',
  styleUrls: ['./piechart.component.css']
})

export class PiechartComponent implements OnInit, OnChanges {

  donedata=[]

  piedata1=[]
  // fvariable:any

  @Input() piedata:any  
  @Input() editdata:any
  @Input() removedata:any
  @Input() piedonefunction:(pdt:any)=>void;
  public dataTable: any  ; 

  @ViewChild('mychart ', {static: false}) mychart;

  // updateToDoChart(){    
  //   this.mychart.draw();
  //   console.log("heloo")
  //   console.log(this.piedata)
  // }
  
  
  




  //  name1=this.piedata1.map((da) => da.name)
  //  age1=this.piedata1.map((da) => da.age)
   
   
   
  
  
  
  public pieChart: GoogleChartInterface = {
    chartType: GoogleChartType.PieChart,
    dataTable: [],
    options: {title: 'PieChart'}

  };


  // piedonefunction(pdt:any){

  //   console.log(this.editdata)
  //   console.log("hello world")

  //   this.piedata=this.editdata
  //   this.donedata=pdt
  //   // console.log(this.donedata)
  // }

  updatepieChart(){    
    this.mychart.draw();
    console.log("heloo")
    console.log(this.piedata)

    this.pieChart={
      chartType:GoogleChartType.PieChart,
      dataTable:[['Student', 'Marks'],
      ['Osama', 36],
      ['Danish', 28],
      ['Ansari', 20]]
    }

    console.log(this.pieChart.dataTable)
  }
  

  // myfunction() {
  //   let ccComponent = this.pieChart.component!;
  //   let ccWrapper = ccComponent.wrapper;

  //   //force a redraw
  //   ccComponent.draw();
  // }


  // select(dataTable){
  //   console.log(this.dataTable)
    
  // }

  // piedonefunction(pdt){  
  //   console.log("hello world")
  //   this.piedata=this.editdata
  //   this.ngOnInit
  //   this.donedata=pdt
  //   console.log(this.piedata)
  // }
 
 
  ngOnInit(): void {
    // this.piedata1=this.piedata
    // console.log(this.piedata1)
    // console.log(this.removedata)
    // console.log("lllllllllllllllpppppppppppppppppppppppppiechartppppppppp",this.editdata)
    // this.removedata=Object.assign([],this.removedata)
    // console.log(this.removedata)

    // this.piedonefuntion(this.donedata)
    // console.log(this.donedata)
    console.log(this.editdata)


    const name1=this.piedata.map((da) => da.name)
    const age1=this.piedata.map((da) => da.age)
    var db=[]

    name1.forEach(item => db.push ([item]));
    let count=0;
    age1.forEach(item =>{
      db[count].push(item)
      count++
    })

    // this.pieChart.dataTable=db;
    
    db.splice(0,0,['Student', 'Marks'])
    this.pieChart.dataTable=db;

    console.log(db)
    console.log(this)
    // this.pieChart.dataTable=this.piedata1;




  }

  ngOnEvent(){}

  ngOnChanges():void{
    console.log(this.removedata)
    // this.piedonefuntion(this.donedata)
    console.log(this.donedata)


    this.piedonefunction(this.donedata)
     
   


    this.updatepieChart()



    console.log("piechart changed usedata",this.editdata)
    // this.piedata=this.editdata
    // console.log(this.piedata)



     if(this.editdata)
     {
      //  console.log("osama")
      //  this.piedata=this.editdata
      //  console.log(this.piedata)
      const name1=this.editdata.map((da) => da.name)
      const age1=this.editdata.map((da) => da.age)
      var db1=[]
  
      name1.forEach(item => db1.push ([item]));
      let count=0;
      age1.forEach(item =>{
        db1[count].push(item)
        count++
      })
  
      db1.splice(0,0,['Student', 'Marks'])
      console.log("onchanges",db1)
      this.pieChart.dataTable=Object.assign([],db1);

    }
    // console.log("onchanges",this.pieChart.dataTable)
    // this.pieChart.dataTable=Object.assign([],db1);

    // this.pieChart.dataTable=db1;
    console.log("onchanges",this.pieChart.dataTable)
  }

  constructor() {
   }

  
}  


