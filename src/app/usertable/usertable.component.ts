import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';

const COLUMNS_SCHEMA = [
  {
    key: 'isSelected',
    type: 'isSelected',
    label: '',
  },
  {
      key: "name",
      type: "text",
      label: "Full Name"
  },
  {
      key: "occupation",
      type: "text",
      label: "Occupation"
  },
  {     
      key: "age",
      type: Number,
      label: "Age"
  },
  {
    key: "isEdit",
    type: "isEdit",
    label: ""
  }
]

@Component({
  selector: 'app-usertable',
  templateUrl: './usertable.component.html',
  styleUrls: ['./usertable.component.css']
})
export class UsertableComponent implements OnInit {
  @Input() data:any;
  @Output() updatedataevent=new EventEmitter() 
  @Output() removeupdateevent=new EventEmitter()
  @Output() pieremoveupdateevent=new EventEmitter()
  @Output() piedoneevent=new EventEmitter()



  
  


  displayedColumns: string[] = COLUMNS_SCHEMA.map((col) => col.key);;
  public dataSource: any  ; 
  columnsSchema: any = COLUMNS_SCHEMA;

  addRow() {
    const newRow = {
      // id: Date.now(),
      name: '',
      occupation: '',
      dateOfBirth: '',
      age: 0,
      isEdit: true,
    };
    this.dataSource = [newRow, ...this.dataSource];
  }
  removeRow(id: number) {
    this.dataSource = this.dataSource.filter((u) => u.id !== id);
  }
  removeSelectedRows() {
    this.dataSource = this.dataSource.filter((u: any) => !u.isSelected);
  }
  update(){
    this.updatedataevent.emit(this.dataSource);
    console.log(this.dataSource)
  }
  removeupdate(){
    this.removeupdateevent.emit(this.dataSource)
  }

  pieremoveupdate(){
    this.pieremoveupdateevent.emit(this.dataSource)

  }

  piedone(){
    this.piedoneevent.emit(this.dataSource);
  }
  

  constructor() { }

  ngOnInit(): void {

    this.dataSource=this.data;
  }

}
