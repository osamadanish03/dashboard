import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgApexchartsModule } from "ng-apexcharts";
import { MatTableModule } from '@angular/material/table'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { OutputGraphComponent } from './output-graph/output-graph.component';
import { TableComponent } from './table/table.component';
import { AgGridModule } from 'ag-grid-angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PiechartComponent } from './piechart/piechart.component';
import { NgChartsModule } from 'ng2-charts';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { HighchartComponent } from './highchart/highchart.component';
import { UsertableComponent } from './usertable/usertable.component';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatNativeDateModule } from '@angular/material/core';
import { LineChartComponent } from './line-chart/line-chart.component';






@NgModule({
  declarations: [
    AppComponent,
    OutputGraphComponent,
    TableComponent,
    PiechartComponent,
    HighchartComponent,
    UsertableComponent,
    LineChartComponent, 
     
  ],




  imports: [
    BrowserModule,
    AppRoutingModule,
    HighchartsChartModule,
    NgApexchartsModule,
    AgGridModule,
    BrowserAnimationsModule,
    MatTableModule,
    NgChartsModule,
    Ng2GoogleChartsModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatDialogModule
    ],


  bootstrap: [AppComponent]
})



export class AppModule { }

