import { Component, OnChanges, OnInit } from '@angular/core';
import * as Highcharts from "highcharts";




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit,OnChanges {
  appeditdata=[];
  appremovedata=[];
  

 USER_DATA = 
[
  {
    id: 1,
    name: 'Osama',
    occupation: 'Developer',
    dateOfBirth: '1984-05-05',
    age: 36,
  },
  {
    id: 2,
    name: 'Danish',
    occupation: 'Developer',
    dateOfBirth: '1992-02-02',
    age: 28,
  },
  {
    id: 3,
    name: 'Ansari',
    occupation: 'HR',
    dateOfBirth: '2000-01-01',
    age: 20,
  },
  {
    id: 4,
    name: 'Raj',
    occupation: 'QA',
    dateOfBirth: '1977-03-03',
    age: 43,
  },
  {
    id: 5,
    name: 'Prem',
    occupation: 'QA',
    dateOfBirth: '1977-03-03',
    age: 40,
  },
  {
    id: 6,
    name: 'Peter',
    occupation: 'Manager',
    dateOfBirth: '1977-03-03',
    age: 45,
  }  
];
 namekey=this.USER_DATA.map(da => da.name)
 agekey=this.USER_DATA.map(da => da.age)





 
  Highcharts: typeof Highcharts = Highcharts;

  chartOptions: Highcharts.Options = {


       xAxis:{
      categories: this.USER_DATA.map(da => da.name),
      title:{
        text:'NAME'
      }, 
   },


   yAxis:{
    title:{ 
      text:'AGE'
    }
  },

    series: [
      {  
        type: "line",
        data: this.USER_DATA.map((da) => da.age), 
      },
    ], 
    

  };

  receiveddata(p){
    this.appeditdata=p
    this.USER_DATA=p;

    for (let index = 0; index < this.appeditdata.length; index++) {
    var agestring=this.appeditdata[index].age
    var agenumber=parseInt(agestring)
    this.appeditdata[index].age=agenumber     
    }
    // var agestring=this.appeditdata[0].age
    // var agenumber=parseInt(agestring)
    // console.log(typeof agenumber)
    // this.appeditdata[0].age=agenumber
  // console.log(typeof this.appeditdata[0].age)

    this.chartOptions={
      xAxis:{
        categories: this.USER_DATA.map(da => da.name),
        title:{
          text:'NAME' 
        }, 
     },
     yAxis:{
      title:{ 
        text:'AGE'
      }
    },


      series: [
        {  
          type: "line",
          data: this.USER_DATA.map((da) => da.age),
        },
      ], 
      
    };
    // var a=this.chartOptions.series.map(Number);

    console.log(this.chartOptions.series[0])

  } 

  receiveremovedata(r){
    this.appremovedata=r
    console.log(this.appremovedata)
    this.USER_DATA=r
    // console.log(r)
    this.chartOptions={
      xAxis:{
        categories: this.USER_DATA.map(da => da.name),
        title:{
          text:'NAME' 
        }, 
     },


     yAxis:{
      title:{ 
        text:'AGE'
      }
    },


      series: [
        {  
          type: "line",
          data: this.USER_DATA.map((da) => da.age),
        },
      ],

    }
  }
  piedonedata=(pdt:any):void =>{
    console.log(pdt)
  }

constructor() {}

ngOnInit(){  
  console.log(this.appeditdata[0].age)
}
ngOnChanges() {  

}

}

