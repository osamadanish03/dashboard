import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import * as Highcharts from "highcharts";


@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit {
  @Input() linedata:any;
  newlinedata=[];


  Highcharts: typeof Highcharts = Highcharts;

  chartOptions: Highcharts.Options = {


       xAxis:{
      categories: this.newlinedata.map(da => da.name),
      title:{
        text:'NAME'
      }, 
   },


   yAxis:{
    title:{ 
      text:'AGE'
    }
  },

    series: [
      {  
        type: "line",
        data: this.newlinedata.map((da) => da.age), 
      },
    ], 
    

  };

  // receiveddata(p){
  //   // this.piedatas=p
  //   this.newlinedata=p;
  //   // console.log("kkkkkkkkPIEDATAkkkkk",this.piedatas);

  //   this.chartOptions={


  //     xAxis:{
  //       categories: this.newlinedata.map(da => da.name),
  //       title:{
  //         text:'NAME' 
  //       }, 
  //    },


  //    yAxis:{
  //     title:{ 
  //       text:'AGE'
  //     }
  //   },


  //     series: [
  //       {  
  //         type: "line",
  //         data: this.newlinedata.map((da) => da.age),
  //       },
  //     ], 
      
  //   };
  //   var a=this.chartOptions.series.map(Number);

  //   // console.log(a)
  //  console.log("ooooooooooooooooooooooooooooooo",p)
  // } 

  // receiveremovedata(r){
  //   this.newlinedata=r
  //   console.log(r)
  //   this.chartOptions={
  //     xAxis:{
  //       categories: this.newlinedata.map(da => da.name),
  //       title:{
  //         text:'NAME' 
  //       }, 
  //    },


  //    yAxis:{
  //     title:{ 
  //       text:'AGE'
  //     }
  //   },


  //     series: [
  //       {  
  //         type: "line",
  //         data: this.newlinedata.map((da) => da.age),
  //       },
  //     ],

  //   }
  // }

  constructor() { }

  ngOnInit(): void {
    this.newlinedata=this.linedata;


    



    console.log(this.linedata)
    console.log(this.newlinedata)

  }

}
