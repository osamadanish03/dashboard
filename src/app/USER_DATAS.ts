export interface USER_DATAS {}

export const USER_data=[
    {
      id: 1,
      name: 'Osama',
      occupation: 'Developer',
      dateOfBirth: '1984-05-05',
      age: 36,
    },
    {
      id: 2,
      name: 'Danish',
      occupation: 'Developer',
      dateOfBirth: '1992-02-02',
      age: 28,
    },
    {
      id: 3,
      name: 'Ansari',
      occupation: 'HR',
      dateOfBirth: '2000-01-01',
      age: 20,
    },
    {
      id: 4,
      name: 'Raj',
      occupation: 'Marketing',
      dateOfBirth: '1977-03-03',
      age: 43,
    },
    {
      id: 5,
      name: 'Prem',
      occupation: 'QA',
      dateOfBirth: '1977-03-03',
      age: 40,
    },
    {
      id: 6,
      name: 'Peter',
      occupation: 'Manager',
      dateOfBirth: '1977-03-03',
      age: 45,
    },
    
  ];